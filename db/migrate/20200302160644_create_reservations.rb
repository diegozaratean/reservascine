class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.string :name
      t.string :celphone
      t.string :identification
      t.string :mail
      t.date :date
      t.integer :movie

      t.timestamps
    end
  end
end
