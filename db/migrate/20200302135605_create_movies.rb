class CreateMovies < ActiveRecord::Migration[6.0]
  def change
    create_table :movies do |t|
      t.string :name
      t.string :description
      t.string :imageurl
      t.date :begindate
      t.date :end

      t.timestamps
    end
  end
end
