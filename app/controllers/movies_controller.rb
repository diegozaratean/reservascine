class MoviesController < ApplicationController
  def index
  	@movies = Movie.all
  end

  def new
  	@movie = Movie.new
	@movie.name = "Demo"
  end

  def create
		@movie = Movie.create(name: params[:movie][:name],description: params[:movie][:description],imageurl: params[:movie][:imageurl],begindate: params[:movie][:begindate],end: params[:movie][:end])
		render json: @movie
  end

end
