class ReservationsController < ApplicationController
  def index
  	@reservations = Reservation.all
  end

  def new
  	@reservation = Reservation.new
	@reservation.name = "Demo"
  end

  def create
		@reservation = Reservation.create(name: params[:reservation][:name],identification: params[:reservation][:identification],celphone: params[:reservation][:celphone],mail: params[:reservation][:mail])
		render json: @reservation
  end

end
