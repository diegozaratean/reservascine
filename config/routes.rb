Rails.application.routes.draw do

  get 'reservations/', to: "reservations#index"
  get 'reservations/new', to: "reservations#new"
  post "reservations", to: "reservations#create"
  
  root to: 'movies#index'


  get "movies/", to: "movies#index"
  get "movies/new", to: "movies#new"
  post "movies", to: "movies#create"
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
